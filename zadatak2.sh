grep -i -E ".*(banana|jabuka|jagoda|dinja|lubenica).*" namirnice.txt

grep -v -i -E ".*(banana|jabuka|jagoda|dinja|lubenica).*" namirnice.txt > ne-voce.txt

# izbrisati sve ispred : jer je to putanja i ime do datoteke\
grep -r -E '([A-Z]{3})([0-9]{6})' ~/projekti/ | sed -E 's/.*:(.*)/\1/'

find . -mtime +7 -mtime -14 -ls

for i in {1..15}; do echo "$i"; done