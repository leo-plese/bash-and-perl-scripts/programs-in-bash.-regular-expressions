if [ "$#" -ne 2 ]
then
    echo "Trebaju 2 argumenta."
    exit 1
fi

if [ ! -e "$1" ]
then
    echo "Dano kazalo ne postoji."
    exit 1
fi


echo "ime kazala="$1""
echo "oblik imena datoteke="$2""
v1="$(find "$(echo $1)" -name "$2")"
test -n "$v1"
if [ "$?" -eq 1 ]
then
    echo "U kazalu "\"$1\"" nema datoteka s imenom oblika "\"$2\""."
    exit 1
fi
echo "$v1" > txtDatoteke.txt

echo "Ukupno datoteka u formatu  '$2': $(wc -l txtDatoteke.txt | sed -E 's/[[:blank:]]*(.*)/\1/' | cut -d " " -f 1)"

sumLines=0

while read r
do
    #echo "-->$r"
    fileLines="$(wc -l "$r")"
    fileLines=$(echo $fileLines | cut -d " " -f 1)
    #echo "==$fileLines"
    sumLines=$((sumLines+fileLines))
done < txtDatoteke.txt
rm txtDatoteke.txt
echo "Ukupno linija = $sumLines"