logDir="./logs"
logs=$(ls $logDir)
for log in $logs
do
	echo ""
	#echo "-> $log"
	if ! echo $log | grep -q -E ".*-02-.*"
	then
		continue
	fi
	#echo $log | sed -n -E "/.*-02-.*/p"
	
	#echo "processing file..."
	Datum=$(echo $log | sed -E "s/.*\.([0-9]+)-([0-9]+)-([0-9]+).*/\3-\2-\1/")
	echo "datum: $Datum"
	echo "---------------------------------------"
	fullPath="$logDir/$log"
	#echo "full=$fullPath"
	sort -k 2 -t '"' $fullPath | sed -E 's/.*"(.*)".*/\1/' > sorlin.txt

	uniq -c sorlin.txt > unilin.txt
	
	sort -r -n -k 1 unilin.txt > unilinsort.txt
    
    #repsum=0
	while read elem
    do
        elemnew=$(echo "$elem" | sed -E 's/([0-9]+) (.*)/  \1 : \2/')
        echo "$elemnew"
		#repsum=$((repsum+$(echo "$elem" | cut -d " " -f 1)))
		####repsum=$((repsum+$(echo "$elemnew" | sed -E 's/([0-9]+).*/\1/')))
    done < unilinsort.txt
    #echo "SUM=$repsum"
done
