proba="Ovo je proba"
echo $proba

# ls * - rekurzivno po poddirektorijima ispise listu datoteka i direktorija\
# -d - sprecava rekurzivno trazenje po poddirektorijima\
# -p - dodaje / na kraj imena direktorija\
# dobivena imena datoteka u tekucem direktoriju ne sadrze / na kraju pa ih se izvlaci s grep\
lista_datoteka=$(ls -d -p * | grep -v /)
echo $lista_datoteka

proba3="$proba. $proba. $proba. "
echo $proba3
# proba3=$(echo $proba | sed -E 's/(.*)/\1. \1. \1. /')

a=4
b=3
c=7
d=$(($(($((a+4)) * b )) % c))
echo $d

broj_rijeci=$(wc -w $(find . -name '*.txt' -type f -maxdepth 1) | grep "total" | sed -E 's/([[:digit:]]+).*/\1/')
echo $broj_rijeci

ls ~

# prvih nekoliko linija su komentari pa se njih odbaci
cut -d ":" -f 1,6,7 /etc/passwd | grep -v '^#'

ps -f | sed -n '2,$p' | sed -E 's/([[:blank:]])[[:blank:]]*/\1/g' | sed -E 's/^[[:blank:]]//' | cut -d " " -f 1,2,8
