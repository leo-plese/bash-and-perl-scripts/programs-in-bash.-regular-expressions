#echo ${!#}
echo $(($#))
if [ "$#" -lt 2 ]
then
    echo "Treba 2 argumenta."
    exit 1
fi

echo "---------"
bakDir="${!#}"
#echo "bakDir=>$bakDir<"
#if [ ! \( \( -e "$bakDir" \) -a \( -d "$bakDir" \) \) ]
if [ ! -e "$bakDir" ]
then
    mkdir "$bakDir"
    echo "Kreirano je kazalo "$bakDir"."
elif [ -f "$bakDir" ]
then
    echo "Postoji datoteka istog imena ($bakDir) kao i kazalo koje se zeli kreirati. Molim drugo ime kazala."
    exit 1
fi
echo "Kopiranje u kazalo..."

i=1
successCopied=0
for arg
do
    if [ $i -eq "$#" ]
    then
        break
    fi
    
    echo "$i: $arg"
    i=$((i+1))
#    if [ ! -f "$arg" ]
#    then
#        echo "$arg je kazalo ili nepostojeca datoteka - preskace..."
#        continue
#    fi
    if [ -d "$arg" ]
    then
        echo "$arg je kazalo - preskace..."
        continue
    fi
    #echo "++++++"
    if [ ! -e "$arg" ]
    then
        echo "Datoteka $arg ne postoji. Nastavlja kopiranje ostalih..."
        continue
    fi
    # chmod 0111 filename.txt - datoteka necitljiva
    if [ ! -r "$arg" ]
    then
        echo "Datoteka $arg nije citljiva. Nastavlja kopiranje ostalih..."
        continue
    fi
    
    # ako je dano ime datoteke koja nije u tekucem direktoriju
    # onda treba odsijeci prvi dio (putanju do datoteke), a ostaviti samo ime datoteke
    echo "CP: $arg -> $bakDir/$(echo "$arg" | sed -E 's/.*\/(.*)/\1/')" 
    cp "$arg" "$bakDir/$(echo "$arg" | sed -E 's/.*\/(.*)/\1/')"
    successCopied=$((successCopied+1))
done
echo "-------"
echo "$successCopied datoteka kopirano je u kazalo $bakDir."