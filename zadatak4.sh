if [ "$#" -ne 2 ]
then
    echo "Trebaju 2 argumenta!"
    exit 1
fi    

picDir="./$(echo $1)"
if [ ! -e "$picDir" ]
then
    echo "Kazalo sa slikama $picDir nije nadjeno!"
    exit 1
fi

sortDir="./$(echo $2)"
if [ ! -e "$sortDir" ]
then
    echo "Kazalo za spremanje slika po mjesecima $sortDir nije nadjeno!"
    exit 1
fi

pics="$(ls $picDir)"
echo "$pics"
ls $picDir > picList.txt

while read pic
do
    picPath="$picDir/$pic"
    #echo "->pic=$picPath"
    dat="$(stat "$picDir/$pic" | cut -d '"' -f 8 | sed -E 's/([[:alpha:]]+).*[[:blank:]]+([0-9]+)/\1 \2/')"
    #echo "dat=$dat"
    yyyy_mm_mod="$(date -jf "%b %Y" "$dat" "+%Y-%m")"
    
    #echo "yyyy_mm_mod=$yyyy_mm_mod"
    ymPicDir="$sortDir/$yyyy_mm_mod"
    #echo "ymPicDir=$ymPicDir"
    if [ ! -e "$ymPicDir" ]
    then
        mkdir "$ymPicDir"
    fi
    mv "$picPath" "$ymPicDir/$pic"
    
    
done < picList.txt

#stat zadatak1.sh | cut -d '"' -f 4 | cut -d " " -f 1,4 | date -jf "%b %Y" "$dat" "+%Y-%m"
